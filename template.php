<?php

/**
 * @file template.php
 */

/**
 * Implementation of HOOK_theme().
 */
function genesis_typo1_theme(&$existing, $type, $theme, $path) {
  $hooks = genesis_theme($existing, $type, $theme, $path);
  // Add your theme hooks like this:
  /*
  $hooks['hook_name_here'] = array( // Details go here );
  */
  return $hooks;
}

/**
 * Override or insert variables into all templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered (name of the .tpl.php file.)
 */
/* -- Delete this line if you want to use this function
function genesis_typo1_preprocess(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
/* -- Delete this line if you want to use this function
function genesis_typo1_preprocess_page(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the node templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
function genesis_typo1_preprocess_node(&$vars, $hook) {
  // Set comment vars for the customised dates.
  $vars['long_date']  = format_date($vars['node']->created, 'custom', "l, F j, Y - H:i");
  $vars['short_date'] = format_date($vars['node']->created, 'custom', "F j, Y");
}
// */

/**
 * Override or insert variables into the comment templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function genesis_typo1_preprocess_comment(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function genesis_typo1_preprocess_block(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Return code that emits a feed icon.
 *
 * @param $url 
 *   The url of the feed.
 * @param $title 
 *   A descriptive title of the feed.
 */
function genesis_typo1_feed_icon($url, $title) {
  $path = drupal_get_path('theme', 'genesis_typo1');
  if ($image = theme('image', $path .'/images/feed-icon-64x64.png', t('Syndicate content'), $title)) {
    return '<a href="'. check_url($url) .'" class="feed-icon">'. $image .'</a>';
  }
}